# Prova wasm

in ../emsdk ci sono gli script e le info per compilare wasm

# Docker 

```bash
# compile with docker image
docker run \
  --rm \
  -v $(pwd):/src \
  -u $(id -u):$(id -g) \
  emscripten/emsdk \
  emcc helloworld.cpp -o helloworld.js

# execute on host machine
node helloworld.js
```


## Compose

```bash
docker compose run --rm debug helloworld.cpp
```